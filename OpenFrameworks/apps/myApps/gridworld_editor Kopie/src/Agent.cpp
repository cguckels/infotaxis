#include "Agent.h"

//Gets current markov model, assuming that diffusion is time invariant
Agent::Agent(Eigen::Vector2i position, std::tr1::shared_ptr<Gridworld> world):position(position), markov(world->getMarkov()){
    //Prepare map: Set cells but the visited to 1, i.e. not visited
    Eigen::Matrix<short, GRID_DIM, GRID_DIM> cells = Eigen::Matrix<short, GRID_DIM, GRID_DIM>::Ones();
    cells(position(0),position(1)) = 0;
    
    //Preprocess markov-matrix, so numbers turn into probabilities
    Eigen::Matrix<float, N_TRANS_STATES, N_TRANS_STATES> p = markov.Zero();
    for(int iX=0; iX<N_TRANS_STATES; iX++){
        float sum = markov.row(iX).sum();
        std::cout << "Row:" << std::endl << markov.row(iX) << std::endl;
        Eigen::Matrix<float, N_TRANS_STATES, 1> r = markov.row(iX) / sum;
        p.row(iX) = markov.row(iX) / sum;
    }
    std::cout << "Normalized markov chain matrix:" << std::endl << p.transpose() << std::endl;
    markov = p;
    
    //calculateSrcProbability();
}

//TODO: Infer probabilities from known states. Update with information about available cells around.

Eigen::Matrix<float, N_TRANS_STATES, 1> Agent::calculateSrcProbability(){
    
    //Normalize source probabilities according to already visited state (set to 0, normalize the rest)
    for(int iX=0; iX<N_TRANS_STATES; iX++){
        
    }
    
    Eigen::Matrix<float, N_TRANS_STATES, 1> p;
    
    //Calculate uniform weight for a yet unseen cell to be the source of diffusion
    float w = 1.0/cells.sum();
    /*
    int iCell = (iX-1)*(GRID_DIM-2)+(iY-1)
    
    //For each cell that could potentially contain the diffusion source
    for(int iX=0; iX<N_TRANS_STATES; iX++){
        float pCell = 0;
        
        for(int iY=0; (iY<N_TRANS_STATES) && (cells(iX,iY)==1); iY++){
            pCell += w * markov(iX,iY);
        }
        p(iX)=pCell;
    }
    
    std::cout << "Estimated src position vector: " << std::endl << p << std::endl;
     
    */
    return p;
    
}

float Agent::calculateEntropy(Eigen::Matrix<float, N_TRANS_STATES, 1> p){
    float entropy = 0;
    
    for(int i=0; i<N_TRANS_STATES; i++){
        entropy += p(i) * log(p(i));
    }
    entropy *= -1;
    
    return entropy;
}


/*
void Agent::move(){
    
    short newX = -1;
    short newY = -1;
    
    //1) Get adjacency information for current position
    Eigen::Vector4i adj = world->getAdjacency(position);
    
    //i) Update own map of the world
    
    //For each possible action, calculate difference in entropy between current estimation of source location and future ones
    //Chose action with the highest information gain
    
    //X) Check if resource found. If not, set new position / mark as seen. Tell GUI.
    cells(newX,newY) = 0;

}*/





