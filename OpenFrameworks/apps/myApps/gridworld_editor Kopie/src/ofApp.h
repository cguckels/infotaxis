#pragma once
#include "ofMain.h"
#include "Gridworld.h"
#include "Agent.h"
#include <Eigen/Dense>
#include <tr1/memory>
#include "definitions.h"

class ofApp : public ofBaseApp{
    
    private:
        const int tileSize = 25;
        const int gapSize = 5;
        int halfWindowWidth;
        bool world_generated = false;
        Eigen::Matrix<short,Eigen::Dynamic,Eigen::Dynamic> cells;
        Eigen::Vector2i srcPosition;
        std::tr1::shared_ptr<Gridworld> world;
        std::tr1::shared_ptr<Agent> agent;
    
	public:
    
		void setup();
		void update();
		void draw();
    
		void keyReleased(int key);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
    
        bool swapCellContent(int x, int y);
        void setDiffusionSource(int iX, int iY);
        void calculateAdjacencyMatrix();
		
};
