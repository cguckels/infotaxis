#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    halfWindowWidth = ofGetWindowWidth()/2;
    srcPosition = Eigen::Vector2i(-1, -1);
    
    Eigen::Matrix<short,Eigen::Dynamic,Eigen::Dynamic> cells;

    cells = Eigen::Matrix<short,GRID_DIM,GRID_DIM>::Zero();
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
    
    //If the world hasn't been defined yet
    if(!world_generated){
        //Legend
        ofDrawBitmapStringHighlight("Left-click cell to make solid. Click again to remove property.", 20, 20);
        ofDrawBitmapStringHighlight("Right-click cell to drop diffusor source. Right-click another cell to move it there.", 20, 40);
        ofDrawBitmapStringHighlight("Type ESC to quit.", 20, 60);

        if(srcPosition(0)<0){
            ofSetColor(150,150,150);
            ofDrawBitmapString("Type S to start pathfinding.", 20, 80);
            ofSetColor(255,255,255);
        }else{
            ofDrawBitmapStringHighlight("Type S to start pathfinding.", 20, 80);
        }

        //Draw gridworld tiles to the middle of the window.
        for(int iX=0; iX<GRID_DIM; iX++){
            for(int iY=0; iY<GRID_DIM; iY++){
                if(cells(iX,iY)==0){
                    ofSetColor(255,255,255);
                }else if(cells(iX,iY)==1){
                    ofSetColor(80,80,80);
                }else{
                    ofSetColor(255,0,255);
                }
                ofRect(halfWindowWidth - (GRID_DIM/2-iX)*(tileSize+gapSize), iY*(tileSize+gapSize) + 100, tileSize, tileSize);
            }
        }
    }else{

        ofDrawBitmapStringHighlight("Type ESC to quit.", 20, 20);

        Eigen::Matrix<float, GRID_DIM, GRID_DIM> concentration = world->getConcentration();
        //Draw gridworld tiles to the middle of the window.
        for(int iX=0; iX<GRID_DIM; iX++){
            for(int iY=0; iY<GRID_DIM; iY++){
                if(cells(iX,iY)==0){
                    ofSetColor(255,255,255);
                }else if(cells(iX,iY)==1){
                    ofSetColor(80,80,80);
                }else{
                    ofSetColor(255,0,255);
                }
                //x,y,z(!),w,h
                ofRect(halfWindowWidth - (GRID_DIM/2-iX)*(tileSize+gapSize), iY*(tileSize+gapSize) + 100, -1, tileSize, tileSize);
            }
        }
        
        //Second layer for concentration gradient
        for(int iX=0; iX<GRID_DIM; iX++){
            for(int iY=0; iY<GRID_DIM; iY++){
                if(cells(iX,iY)==0){
                    float c = concentration(iX,iY);
                    ofSetColor(255, 0, 255, c*255);
                    ofRect(halfWindowWidth - (GRID_DIM/2-iX)*(tileSize+gapSize), iY*(tileSize+gapSize) + 100, 0, tileSize, tileSize);
                }
            }
        }
        
        

        
    }

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    if ((key == 's' || key == 'S') && !world){
        //Only execute if diffusion source had been specified
        if(!(srcPosition(0)<0)){
            //Initialise world object (i.e. calculate concentration, etc.)
            world = std::tr1::shared_ptr<Gridworld>(new Gridworld(cells, srcPosition));
            
            //TODO: Give user a chance to set this explicitly
            Eigen::Vector2i agentPosition;
            agentPosition(0) = 2;
            agentPosition(1) = 2;
            
            agent = std::tr1::shared_ptr<Agent>(new Agent(agentPosition, world));
            
            //ofSleepMillis(5000);
            world_generated = true;
        }
    }
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
    //Traceback to tile index
    int space = tileSize + gapSize;
    float iX = x-(halfWindowWidth)-((GRID_DIM/2)*space)*-1;
    iX = floor(iX/space);
    float iY = floor((y-100)/space);
    
    if(((-1<iX)&&(iX<GRID_DIM)) && ((-1<iY)&&(iY<GRID_DIM))){
        //LMB
        if(button==0){
            swapCellContent((int)iX, (int)iY);
        //RMB
        }else if(button==2){
            setDiffusionSource(iX, iY);
        }
    }
    return false;
}

//Swap cell content indicators, i.e. if 0->1, if 1->0. If any other number, do not swap and return false
bool ofApp::swapCellContent(int iX, int iY){
    
    if(cells(iX,iY)<2){
        cells(iX,iY)=abs(cells(iX,iY)-1);
        return true;
    }
    return false;
    
}

void ofApp::setDiffusionSource(int iX, int iY){
    
    //If source has already been set, remove the old instance
    if(srcPosition(0)>0){
        cells(srcPosition(0),srcPosition(1))=0;
    }
    
    srcPosition(0) = iX;
    srcPosition(1) = iY;
    cells(iX,iY)=2;

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    halfWindowWidth = ofGetWindowWidth()/2;
    draw();
}
