#pragma once
#include <stdio.h>
#include <Eigen/Dense>
#include <iostream>
#include <tr1/memory>
#include "definitions.h"
#include "Gridworld.h"

class Agent{
    
private:
    Eigen::Vector2i position;
    Eigen::Matrix<short, GRID_DIM, GRID_DIM> cells;
    Eigen::Matrix<float, GRID_DIM, GRID_DIM> srcProbability;
    Eigen::Matrix<float, N_TRANS_STATES, N_TRANS_STATES> markov;
    std::tr1::shared_ptr<Gridworld> world;

    Eigen::Matrix<float, N_TRANS_STATES, 1> calculateSrcProbability();    
    float calculateEntropy(Eigen::Matrix<float, N_TRANS_STATES, 1> p);

public:
    Agent(Eigen::Vector2i position, std::tr1::shared_ptr<Gridworld> world);
    Eigen::Vector2i getPosition();
    
};
