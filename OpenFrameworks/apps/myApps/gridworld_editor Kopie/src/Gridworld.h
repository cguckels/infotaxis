#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <Eigen/Dense>
#include <Eigen/LU>
#include "definitions.h"


class Gridworld{
    
private:
    std::map<std::vector<int>, int > coordsToId;
    
    Eigen::Matrix<short,Eigen::Dynamic,Eigen::Dynamic> cells;
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> concentration;
    
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> transitions;
    //Eigen::Matrix<float, N_STATES, N_STATES>  adjacency;
    Eigen::Matrix<float, N_TRANS_STATES, N_TRANS_STATES> markov;
    Eigen::Vector2i srcPosition;
    
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> calculateConcentration(int weight);
    Eigen::Matrix<float, N_STATES, N_STATES> calculateTransitionMatrix();
    //Eigen::Matrix<float, N_STATES, N_STATES> calculateAdjacencyMatrix();
    Eigen::Matrix<float, N_TRANS_STATES, N_TRANS_STATES> calculateMarkovChainMatrix();
    
public:
    Gridworld(Eigen::Matrix<short, GRID_DIM, GRID_DIM> cells, Eigen::Vector2i srcPosition);
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic>  getTransitions();
    Eigen::Matrix<float, GRID_DIM, GRID_DIM> getConcentration();
    Eigen::Matrix<float, N_TRANS_STATES, N_TRANS_STATES> getMarkov();
    Eigen::Vector4i getAdjacency(Eigen::Vector2i position);
};

