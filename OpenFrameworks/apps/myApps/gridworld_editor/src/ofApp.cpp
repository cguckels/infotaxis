#include "ofApp.h"

//Main Open Frameworks class for visualisation
void ofApp::setup(){
    halfWindowWidth = ofGetWindowWidth()/2;
    srcPosition = Eigen::Vector2i(-1, -1);
    agentPosition = Eigen::Vector2i(-1, -1);
    
    cells = Eigen::Matrix<short, Eigen::Dynamic, Eigen::Dynamic>::Zero(GRID_DIM,GRID_DIM);
}

void ofApp::drawLegend(){
    //Legend
    ofDrawBitmapStringHighlight("Left-click cell to make solid. Click again to remove property.", 20, 20);
    ofDrawBitmapStringHighlight("Middle-click cell to place agent. Right-click another cell to move it there. Agents cannot be placed on walls or the diffusor source.", 20, 40);
    ofDrawBitmapStringHighlight("Right-click cell to drop diffusor source. Right-click another cell to move it there.", 20, 60);
    ofDrawBitmapStringHighlight("Type ESC to quit.", 20, 80);
    
    if(srcPosition(0)>0 && agentPosition(0)>0){
        ofDrawBitmapStringHighlight("Type S to start pathfinding.", 20, 100);
    }else{
        ofSetColor(150,150,150);
        ofDrawBitmapString("Type S to start pathfinding.", 20, 100);
        ofSetColor(255,255,255);
    }
}

void ofApp::drawGridworld(){
    
    //Draw gridworld tiles to the middle of the window.
    for(int iX=0; iX<GRID_DIM; iX++){
        for(int iY=0; iY<GRID_DIM; iY++){
            if(cells(iX,iY)==0){
                ofSetColor(255,255,255);
            }else if(cells(iX,iY)==1){
                ofSetColor(80,80,80);
            }else{
                ofSetColor(255,0,255);
            }
            //x,y,z(!),w,h
            ofRect(halfWindowWidth - (GRID_DIM/2-iX)*(tileSize+gapSize), iY*(tileSize+gapSize) + 100, -1, tileSize, tileSize);
        }
    }
    
    if(agentPosition(0)>-1){
        ofSetColor(0,0,255);
        int x = halfWindowWidth - (GRID_DIM/2-agentPosition(0))*(tileSize+gapSize)+tileSize/2.0;
        int y = agentPosition(1)*(tileSize+gapSize) + 100 +tileSize/2.0;
        ofCircle(x, y, 1, tileSize/4.0);
    }
}

void ofApp::drawGradient(){
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> concentration = world->getConcentration();
    
    //Second layer for concentration gradient
    for(int iX=0; iX<GRID_DIM; iX++){
        for(int iY=0; iY<GRID_DIM; iY++){
            if(cells(iX,iY)==0){
                float c = concentration(iX,iY);
                ofSetColor(255, 0, 255, c*255);
                ofRect(halfWindowWidth - (GRID_DIM/2-iX)*(tileSize+gapSize), iY*(tileSize+gapSize) + 100, 0, tileSize, tileSize);
            }
        }
    }
}

//Main visualisation function to draw the grid and legend
void ofApp::draw(){
    
    //If the world hasn't been defined yet
    if(!world_generated){
        drawLegend();
        drawGridworld();
    }else{
        ofDrawBitmapStringHighlight("Type ESC to quit.", 20, 20);
        drawGridworld();
        drawGradient();
    }
}

//Key event listener
void ofApp::keyReleased(int key){
    if ((key == 's' || key == 'S') && !world){
        //Only execute if diffusion source had been specified
        if(!(srcPosition(0)<0)){
            //Initialise world object (i.e. calculate concentration, etc.)
            world = std::tr1::shared_ptr<Gridworld>(new Gridworld(cells, srcPosition));
            
            agent = std::tr1::shared_ptr<Agent>(new Agent(agentPosition, world));
            //agent->move(1);
            
            world_generated = true;
        }
    }
}

//Mouse event listener
void ofApp::mouseReleased(int x, int y, int button){
    
    //Traceback to tile index
    int space = tileSize + gapSize;
    float iX = x-(halfWindowWidth)-((GRID_DIM/2)*space)*-1;
    iX = floor(iX/space);
    float iY = floor((y-100)/space);
    
    if(((-1<iX)&&(iX<GRID_DIM)) && ((-1<iY)&&(iY<GRID_DIM))){
        //LMB
        if(button==0){
            swapCellContent((int)iX, (int)iY);
        //RMB
        }else if(button==1){
            //Do not allow to place the agent on walls/the source
            if(!(cells(iX,iY)==1 || cells(iX,iY)==2)){
                agentPosition(0) = iX;
                agentPosition(1) = iY;
            }
        }else if(button==2){
            setDiffusionSource(iX, iY);
        }
    }
    return false;
}

//Swap cell content indicators, i.e. if 0->1, if 1->0. If any other number, do not swap and return false
bool ofApp::swapCellContent(int iX, int iY){
    
    if(cells(iX,iY)<2){
        cells(iX,iY)=abs(cells(iX,iY)-1);
        return true;
    }
    return false;
    
}

//Set the diffusion source to cell iX, iY and remove the old instance from the grid
void ofApp::setDiffusionSource(int iX, int iY){
    
    if(srcPosition(0)>0){
        cells(srcPosition(0),srcPosition(1))=0;
    }
    
    srcPosition(0) = iX;
    srcPosition(1) = iY;
    cells(iX,iY)=2;

}

//Calculate window dimensions again if resized and trigger draw procedure to center grid
void ofApp::windowResized(int w, int h){
    halfWindowWidth = ofGetWindowWidth()/2;
    draw();
}
