#include "Agent.h"

Agent::Agent(Eigen::Vector2i position, std::tr1::shared_ptr<Gridworld> world):position(position), markov(world->getMarkov()), world(world){
    
    //Set entropy initially to the maximum
    entropy = log2f(N_TRANS_STATES);
    
    //Prepare map: Set cells but the visited to 1, i.e. not visited
    cells = Eigen::Matrix<short, Eigen::Dynamic, Eigen::Dynamic>::Ones(GRID_DIM,GRID_DIM);
    cells(position(0),position(1)) = 0;
    
    //Preprocess markov-matrix, so numbers turn into probabilities
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> p = markov.Zero(N_TRANS_STATES, N_TRANS_STATES);
    for(int iX=0; iX<N_TRANS_STATES; iX++){
        float sum = markov.row(iX).sum();
        //std::cout << "Row:" << std::endl << markov.row(iX) << std::endl;
        Eigen::Matrix<float, N_TRANS_STATES, 1> r = markov.row(iX) / sum;
        p.row(iX) = markov.row(iX) / sum;
    }
    std::cout << "Normalized markov chain matrix:" << std::endl << p.transpose() << std::endl;
    markov = p;
    
}

//Estimates the probability distribution of the diffusion source location
Eigen::Matrix<float, Eigen::Dynamic, 1> Agent::calculateSrcProbability(){
    
    //Check which transient states have been visited so far, and what their id is
    std::set<int> visitedIds;
    for(int iX=1; iX<GRID_DIM-1; iX++){
        for(int iY=1; iY<GRID_DIM-1; iY++){
            if(cells(iX,iY)==0){
                //Calcualte id relative to markov matrix
                int id = (iX-1)*(GRID_DIM-2)+(iY-1);
                visitedIds.insert(id);
            }
        }
    }
    
    std::cout << "Markov chain after removing element:" << std::endl << markov.transpose() << std::endl;
    
    //Normalize source probabilities according to already visited state (set to 0, normalize the rest)
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> p = markov.Zero(N_TRANS_STATES, N_TRANS_STATES);
    std::set<int>::iterator it;
    for(int iX=0; iX<N_TRANS_STATES; iX++){
        for(it = visitedIds.begin(); it!= visitedIds.end(); ++it){
            int iY = *it;
            markov(iX,iY)=0; //Visited, not source location, thus set probability to 0
        }
        
        //Normalize remaining probabilities, so they sum up to 1
        float sum = markov.row(iX).sum();
        Eigen::Matrix<float, N_TRANS_STATES, 1> r = markov.row(iX) / sum;
        p.row(iX) = markov.row(iX) / sum;
    }
    markov = p;
    
    std::cout << "Normalized markov chain after removing element:" << std::endl << p.transpose() << std::endl;
    
    //Calculate uniform weight for a yet unseen cell to be the source of diffusion
    float w = 1.0/(N_TRANS_STATES-visitedIds.size());
    
    //For each yet unvisited cell that could potentially contain the diffusion source
    Eigen::Matrix<float, Eigen::Dynamic, 1> estimatedLocation = markov.Zero(N_TRANS_STATES, 1);
    for(int iX=0; iX<N_TRANS_STATES; iX++){
        if(visitedIds.find(iX)!=visitedIds.end()){
            continue;
        }
        float pCell = 0;
        for(int iY=0; (iY<N_TRANS_STATES); iY++){
            pCell += w * markov(iX,iY);
        }
        estimatedLocation(iX)=pCell;
    }
    
    std::cout << "Estimated src position vector: " << std::endl << estimatedLocation << std::endl;
    
    return estimatedLocation;
    
}

//Calculates the Shannon-Entropy
float Agent::calculateEntropy(Eigen::Matrix<float, Eigen::Dynamic, 1> p){
    float entropy = 0;
    
    for(int i=0; i<N_TRANS_STATES; i++){
        if(p(i)>0){
            entropy += p(i) * log2f(p(i));
        }
    }
    entropy *= -1;
    
    return entropy;
}

//Essential method to make the agent move one step, based on Infotaxis
void Agent::move(){
 
    //Update the probability distribution for the source location, given the (new) current location
    srcProbability = calculateSrcProbability();
    
    //Update the entropy for that distribution
    entropy = calculateEntropy(srcProbability);
    
    //Get adjacency information for current position: which states can be reached from here?
    Eigen::Vector4i adj = world->getAdjacency(position);

    //Calculate expected action gain for each available transition into an adjacent state
    short newX = -1;
    short newY = -1;
    int id = -1;
    float entropyDelta[4] = {4,log2f(N_TRANS_STATES)};
    
    //Left
    if (adj(0)==1){
        newX = position(0)-1;
        newY = position(1);
        id = (newX-1)*(GRID_DIM-2)+(newY);
        entropyDelta[0]=(srcProbability(id) * entropy * -1) + ((1-srcProbability(id))*entropy);
    }
    
    //right
    if (adj(1)==1){
        newX = position(0)+1;
        newY = position(1);
        id = (newX+1)*(GRID_DIM-2)+(newY);
        entropyDelta[1]=(srcProbability(id) * entropy * -1) + ((1-srcProbability(id))*entropy);
    }
    
    //up
    if (adj(2)==1){
        newX = position(0);
        newY = position(1)-1;
        id = (newX)*(GRID_DIM-2)+(newY-1);
        entropyDelta[2]=(srcProbability(id) * entropy * -1) + ((1-srcProbability(id))*entropy);
    }
    
    //down
    if (adj(3)==1){
        newX = position(0);
        newY = position(1)+1;
        id = (newX)*(GRID_DIM-2)+(newY);
        entropyDelta[3]=(srcProbability(id) * entropy * -1) + ((1-srcProbability(id))*entropy);
    }

    //Determine which action leads to highest expected information gain
    int maxIndex = std::distance(entropyDelta, std::max_element(entropyDelta, entropyDelta + 4));

    //Perform action/set new state
    if(maxIndex==0){
        position(0)=position(0)-1;
    }
    if(maxIndex==1){
        position(0)=position(0)+1;
    }
    if(maxIndex==2){
        position(1)=position(1)-1;
    }
    if(maxIndex==3){
        position(1)=position(1)+1;
    }
    
    //Update own map of the world
    cells(position(0),position(1)) = 0;

}

//Move the agent an arbitrary number of steps. Returns path moved or the same path if source location reached.
std::vector<Eigen::Vector2i> Agent::move(int steps){
    
    std::vector<Eigen::Vector2i> path;
    path.push_back(position);
    for(int i=0; i<steps; i++){
        //If agent at source location, do nothing
        if(cells(position(0),position(1))==2){
            return path;
        }
        move();
        path.push_back(position);
    }
    
    return path;
    
}

