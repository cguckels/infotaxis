#pragma once
#include <stdio.h>
#include <Eigen/Dense>
#include <iostream>
#include <tr1/memory>
#include <set>
#include <algorithm>    // std::min_element, std::max_element
#include "definitions.h"
#include "Gridworld.h"

class Agent{
    
private:
    Eigen::Vector2i position;
    Eigen::Matrix<short,Eigen::Dynamic,Eigen::Dynamic>  cells;
    Eigen::Matrix<float, Eigen::Dynamic, 1> srcProbability;
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> markov;
    std::tr1::shared_ptr<Gridworld> world;
    float entropy;

    Eigen::Matrix<float, Eigen::Dynamic, 1> calculateSrcProbability();
    float calculateEntropy(Eigen::Matrix<float, Eigen::Dynamic, 1> p);

public:
    Agent(Eigen::Vector2i position, std::tr1::shared_ptr<Gridworld> world);
    Eigen::Vector2i getPosition();
    void move();
    std::vector<Eigen::Vector2i> move(int steps);
};
