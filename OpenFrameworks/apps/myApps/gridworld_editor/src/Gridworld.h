#pragma once
#include <iostream>
#include <vector>
#include <map>
#include <Eigen/Dense>
#include <Eigen/LU>
#include "definitions.h"


class Gridworld{
    
private:
    std::map<std::vector<int>, int > coordsToId;
    Eigen::Matrix<short,Eigen::Dynamic,Eigen::Dynamic> cells;
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> concentration;
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> transitions;
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> markov;
    Eigen::Vector2i srcPosition;
    
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> calculateConcentration(int weight);
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> calculateTransitionMatrix();
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> calculateMarkovChainMatrix();
    
public:
    Gridworld(Eigen::Matrix<short,Eigen::Dynamic,Eigen::Dynamic> cells, Eigen::Vector2i srcPosition);
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> getTransitions();
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> getConcentration();
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> getMarkov();
    Eigen::Vector4i getAdjacency(Eigen::Vector2i position);
};

