#include "Gridworld.h"

Gridworld::Gridworld(Eigen::Matrix<short,Eigen::Dynamic,Eigen::Dynamic> c, Eigen::Vector2i srcPosition):cells(c), srcPosition(srcPosition){
    //First calculate the transition matrix for the gridworld states,
    //then transform it into an absorbing markov chain matrix and use the latter to calculate the diffusion concentration in each cell of the grid.
    transitions = calculateTransitionMatrix();
    markov = calculateMarkovChainMatrix();
    concentration = calculateConcentration(12);
}

/*
 * Create transition matrix between states in canonical form
 * Distinguish absorbing states (edges of the world) and transient states
 * Canonical form: start with absorbing states.
 */
Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> Gridworld::calculateTransitionMatrix(){
    
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> transitions = Eigen::MatrixXf::Zero(N_STATES,N_STATES);
    
    coordsToId = std::map<std::vector<int>, int >();
    
    //Absorbing states (boundaries)
    int id = 0;
    std::vector<int> crds(2);
    for(int iX=0; iX<GRID_DIM; iX++){
        //top edge
        crds[0]=iX;
        crds[1]=0;
        coordsToId[crds]=id;
        transitions(id,id)=1;
        id++;
        
        //bottom edge
        crds[0]=iX;
        crds[1]=GRID_DIM-1;
        coordsToId[crds]=id;
        transitions(id,id)=1;
        id++;
    }
    
    for(int iY=1; iY<(GRID_DIM-1); iY++){
        //left edge
        crds[0]=0;
        crds[1]=iY;
        coordsToId[crds]=id;
        transitions(id,id)=1;
        id++;
        
        //right edge
        crds[0]=GRID_DIM-1;
        crds[1]=iY;
        coordsToId[crds]=id;
        transitions(id,id)=1;
        id++;
    }

    //Map all remaining cell coordinates to a unique id
    for(int iX=1; iX<(GRID_DIM-1); iX++){
        for(int iY=1; iY<(GRID_DIM-1); iY++){
            crds[0]=iX;
            crds[1]=iY;
            coordsToId[crds]=id;
            id++;
        }
    }

    //Transient states
    int idAdj;
    for(int iX=1; iX<GRID_DIM-1; iX++){
        for(int iY=1; iY<GRID_DIM-1; iY++){
            if(cells(iX,iY)==1){
                continue;
            }
            
            crds[0]=iX;
            crds[1]=iY;
            id = coordsToId[crds];
            
            //Check number of adjacent cells
            int nAdj=0;
            if(iX > 0 && cells(iX-1,iY)%2==0){
                nAdj++;
            }
            if(iX < (GRID_DIM-1) && cells(iX+1,iY)%2==0){
                nAdj++;
            }
            if(iY > 0 && cells(iX,iY-1)%2==0){
                nAdj++;
            }
            if(iY < (GRID_DIM-1) && cells(iX,iY+1)%2==0){
                nAdj++;
            }
            float weight = 1.0/nAdj;
            
            //Self-referencing
            /*
            if(cells(iX,iY)%2==0){
                transitions(id,id)=1;
            }*/
            
            //left
            if(iX > 0 && cells(iX-1,iY)%2==0){
                crds[0]=iX-1;
                crds[1]=iY;
                idAdj = coordsToId[crds];
                transitions(id,idAdj)=weight;
            }
            
            //right
            if(iX < (GRID_DIM-1) && cells(iX+1,iY)%2==0){
                crds[0]=iX+1;
                crds[1]=iY;
                idAdj = coordsToId[crds];
                transitions(id,idAdj)=weight;
            }
            
            //up
            if(iY > 0 && cells(iX,iY-1)%2==0){
                crds[0]=iX;
                crds[1]=iY-1;
                idAdj = coordsToId[crds];
                transitions(id,idAdj)=weight;
            }
            
            //down
            if(iY < (GRID_DIM-1) && cells(iX,iY+1)%2==0){
                crds[0]=iX;
                crds[1]=iY+1;
                idAdj = coordsToId[crds];
                transitions(id,idAdj)=weight;
            }
        }
    }
    return transitions;
}

//Calculates the markov for the absorbing markov chain resulting from the transition matrix
Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> Gridworld::calculateMarkovChainMatrix(){
    
    //Create new matrix for non-absorbing state transitions
    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> markov;
    
    //Extract transitions between non-absorbing (=transient) states out of matrix
    markov = transitions.block(N_ABS_STATES,N_ABS_STATES,N_TRANS_STATES,N_TRANS_STATES).matrix();
    std::cout << "Transitions matrix:" << std::endl << transitions << std::endl;

    //Substract identity matrix and calculate inverse
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> ident = markov.Identity(N_TRANS_STATES, N_TRANS_STATES);
    markov = ident - markov;
    std::cout << "Transient sub-matrix:" << std::endl << markov.transpose() << std::endl;
   
    float det = markov.determinant();
    std::cout << "Determinant: " << det << std::endl;
    if(det==0){
        std::cout << "Markov chain matrix is singular - cannot calculate inverse. Please modify and retry. ";
        exit(1);
    }
    
    markov = markov.inverse().eval();
    std::cout  << "Markov chain matrix:" << std::endl << markov << std::endl;
        
    return markov;
    
}

//Calculate the diffusion concentration in each transient cell of the grid
Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> Gridworld::calculateConcentration(int weight){

    Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> concentration = Eigen::MatrixXf::Zero(GRID_DIM,GRID_DIM);
    
    int iSrc = (srcPosition(0)-1)*(GRID_DIM-2)+(srcPosition(1)-1);
    for(int iX=1; iX<GRID_DIM-1; iX++){
        for(int iY=1; iY<GRID_DIM-1; iY++){
            int iCell = (iX-1)*(GRID_DIM-2)+(iY-1);
            concentration(iX,iY) = markov(iSrc,iCell);//weight * markov(iSrc,iCell);
        }
    }

    std::cout <<  "Concentration matrix:" << std::endl << concentration.transpose() << std::endl;
    
    return concentration;
}

Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> Gridworld::getTransitions(){
    return transitions;
}

Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> Gridworld::getConcentration(){
    return concentration;
}

Eigen::Matrix<float,Eigen::Dynamic,Eigen::Dynamic> Gridworld::getMarkov(){
    return markov;
}

//Return adjacency vector for current position, indicating accessibility of (left, right, up, down) cell
Eigen::Vector4i Gridworld::getAdjacency(Eigen::Vector2i position){
    
    Eigen::Vector4i adj = Eigen::Vector4i::Zero();
    int iX=position(0);
    int iY=position(1);
        
    //left
    std::cout << cells << std::endl;
    if(iX > 0 && cells(iX-1,iY)%2==0){
        adj(0)=1;
    }
    
    //right
    if(iX < (GRID_DIM-1) && cells(iX+1,iY)%2==0){
        adj(1)=1;
    }
    
    //up
    if(iY > 0 && cells(iX,iY-1)%2==0){
        adj(2)=1;
    }
    
    //down
    if(iY < (GRID_DIM-1) && cells(iX,iY+1)%2==0){
        adj(3)=1;
    }
    
    return adj;
    
}