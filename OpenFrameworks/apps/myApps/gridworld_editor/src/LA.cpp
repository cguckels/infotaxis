#include "LA.h"

LA::LA() {}

LA::~LA() {}

//Matrices must have same size!
std::vector<std::vector<float> > LA::substractMatrices(std::vector<std::vector<float> > m1, std::vector<std::vector<float> > m2){
    int nX = m1.size();
    int nY = m1[0].size();
    std::vector<std::vector<float> > m3(nX, std::vector<float>(nY));
    
    for(int iX=0;iX<nX;iX++){
        for(int iY=0;iY<nY;iY++){
            m3[iX][iY] = m1[iX][iY]-m2[iX][iY];
        }
    }
    
    return m3;
    
}
