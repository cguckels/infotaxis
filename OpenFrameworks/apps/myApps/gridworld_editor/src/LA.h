#pragma once
#include <vector>
#include <iostream>
#include <iomanip>

class LA{
private:
    
public:
    //Constructor and Destructor
    LA();
    virtual ~LA();
    static std::vector<std::vector<float> > substractMatrices(std::vector<std::vector<float> > m1, std::vector<std::vector<float> > m2);
    
};
